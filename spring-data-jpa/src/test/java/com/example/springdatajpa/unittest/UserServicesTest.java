package com.example.springdatajpa.unittest;

import com.example.springdatajpa.dtos.CreateUserRequestDTO;
import com.example.springdatajpa.dtos.CreateUserResponseDTO;
import com.example.springdatajpa.entity.User;
import com.example.springdatajpa.exception.DataAlreadyExistException;
import com.example.springdatajpa.exception.ValidationErrorException;
import com.example.springdatajpa.repository.UserRepository;
import com.example.springdatajpa.services.UserServices;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServicesTest {

    @Mock
    UserRepository userRepository;

    ModelMapper modelMapper = spy(new ModelMapper());

    @InjectMocks
    UserServices userServicesUnderTest = spy(new UserServices());

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void givenValidRequest_whenCreatedNewUser_thenShouldBeCreated() {

        CreateUserRequestDTO requestDTO = new CreateUserRequestDTO();
        requestDTO.setName("farhan");
        requestDTO.setAge(22);
        requestDTO.setEmail("farhansyahputra10@gmail.com");

        when(userRepository.findById(anyLong())).thenReturn(Optional.empty());

        User user = modelMapper.map(requestDTO, User.class);
        user.setId(1L);

        when(userRepository.save(any(User.class))).thenReturn(user);

        CreateUserResponseDTO responseDTO = userServicesUnderTest.createUser(requestDTO);
        assertThat(responseDTO.getId()).isNotNull();
        assertThat(responseDTO.getName()).isEqualTo(requestDTO.getName());
        assertThat(responseDTO.getEmail()).isEqualTo(requestDTO.getEmail());
        assertThat(responseDTO.getAge()).isEqualTo(requestDTO.getAge());
    }

    @Test(expected = DataAlreadyExistException.class)
    public void givenValidRequestAndUserAlreadyExists_whenCreatedNewUser_thenShouldThrowException() {
        CreateUserRequestDTO requestDTO = new CreateUserRequestDTO();
        requestDTO.setName("farhan");
        requestDTO.setAge(22);
        requestDTO.setEmail("farhansyahputra10@gmail.com");

        User user = modelMapper.map(requestDTO, User.class);
        user.setId(1L);

        when(userRepository.findByName(anyString())).thenReturn(Optional.of(user));
        userServicesUnderTest.createUser(requestDTO);
    }

    @Test(expected = ValidationErrorException.class)
    public void givenNullRequest_whenCreatedNewUser_thenThrowErrorException() {
        userServicesUnderTest.createUser(null);
    }
}
