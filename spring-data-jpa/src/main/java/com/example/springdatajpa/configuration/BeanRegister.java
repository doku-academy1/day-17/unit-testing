package com.example.springdatajpa.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.modelmapper.ModelMapper;;

@Configuration
public class BeanRegister {

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
}
