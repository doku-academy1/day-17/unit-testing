package com.example.springdatajpa.controller;

import com.example.springdatajpa.dtos.CreateUserRequestDTO;
import com.example.springdatajpa.entity.User;
import com.example.springdatajpa.repository.OrderRespository;
import com.example.springdatajpa.services.UserServices;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {

//    @Autowired
//    OrderRespository orderRespository;
    @Autowired
    UserServices userServices;

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully get users")
    })
    @GetMapping("/users")
    public ResponseEntity<List<User>> getAllUsers() {
        return new ResponseEntity<>(userServices.getAllUsers(), HttpStatus.OK) ;
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully get users by id")
    })
    @GetMapping("/users/{id}")
    public ResponseEntity<User> getUser(@PathVariable("id") long id) {
        return new ResponseEntity<>(userServices.getUserById(id), HttpStatus.OK);
    }

    @ApiResponses(value = {
            @ApiResponse(code = 202, message = "Successfully delete user")
    })
    @DeleteMapping("/users/{id}")
    public ResponseEntity deleteUser(@PathVariable("id") long id) {
        userServices.deleteUser(id);
        return new ResponseEntity<>("User deleted successfully!" ,HttpStatus.ACCEPTED);
    }

    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully create users")
    })
    @PostMapping("/users")
    public ResponseEntity<String> addUser(@RequestBody CreateUserRequestDTO user) {
        userServices.createUser(user);
        return new ResponseEntity<>("User created successfully!", HttpStatus.CREATED);
    }

    @ApiResponses(value = {
            @ApiResponse(code = 202, message = "Successfully edit user")
    })
    @PutMapping("/users/{id}")
    public ResponseEntity<User> updateUser(@RequestBody User user, @PathVariable long id) {
        userServices.updateUser(user, id);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }
}
