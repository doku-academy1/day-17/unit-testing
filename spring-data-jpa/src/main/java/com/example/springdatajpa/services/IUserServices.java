package com.example.springdatajpa.services;

import com.example.springdatajpa.dtos.CreateUserRequestDTO;
import com.example.springdatajpa.dtos.CreateUserResponseDTO;

public interface IUserServices {
    CreateUserResponseDTO createUser(CreateUserRequestDTO requestDTO);
}
