package com.example.springdatajpa.services;

import com.example.springdatajpa.dtos.CreateUserRequestDTO;
import com.example.springdatajpa.dtos.CreateUserResponseDTO;
import com.example.springdatajpa.entity.User;
import com.example.springdatajpa.exception.DataAlreadyExistException;
import com.example.springdatajpa.exception.ValidationErrorException;
import com.example.springdatajpa.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserServices implements IUserServices {
    @Autowired
    UserRepository userRepository;

    @Autowired
    ModelMapper modelMapper;

    public User getUserById(long id) {
        return userRepository.findById(id).get();
    }

    public List<User> getAllUsers() {
        List<User> users = new ArrayList<User>();
        userRepository.findAll().forEach(user -> users.add(user));
        return users;
    }

    public CreateUserResponseDTO createUser(CreateUserRequestDTO requestDTO) {
        validate(requestDTO);
        User user = convertToEntity(requestDTO);

        Optional<User> userOptional = userRepository.findByName(requestDTO.getName());
        if(userOptional.isPresent()) {
            throw new DataAlreadyExistException("Data with name=(" + requestDTO.getName() + ") already exists.");
        }

        User createdUser = userRepository.save(user);
        return convertToDto(createdUser);
    }

    public void updateUser(User users, Long id) {
        userRepository.findById(id).map(user -> {
            user.setAge(users.getAge());
            user.setName(users.getName());
            user.setEmail(users.getEmail());
            return userRepository.save(user);
        }).orElseGet(() -> {return userRepository.save(users);});
    }

    public void deleteUser(long id) {
        userRepository.deleteById(id);
    }

    private User convertToEntity(CreateUserRequestDTO requestDTO) {
        return modelMapper.map(requestDTO, User.class);
    }
    private CreateUserResponseDTO convertToDto(User user) {
        return modelMapper.map(user, CreateUserResponseDTO.class);
    }

    private void validate(CreateUserRequestDTO requestDTO) {
        if (requestDTO == null) {
            throw new ValidationErrorException("Body request cannot be empty.");
        }
    }
}
