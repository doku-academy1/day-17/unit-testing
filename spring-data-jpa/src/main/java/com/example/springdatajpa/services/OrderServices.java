package com.example.springdatajpa.services;

import com.example.springdatajpa.entity.Order;
import com.example.springdatajpa.repository.OrderRespository;
import com.example.springdatajpa.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OrderServices {

    @Autowired
    UserRepository userRepository;
    @Autowired
    OrderRespository orderRespository;

    public List<Order> getOrderByUserId(Long user_id) {
        List<Order> orders = orderRespository.findByUserId(user_id);
        return orders;
    }

    public Map<String, Object> getAll(Integer page, Integer size) {
        Page<Order> pageOrder;

        Map<String, Object> response = new HashMap<>();

        if(page == null || size == null) {

            pageOrder = orderRespository.findAll();
            List<Order> orders = pageOrder.getContent();
//            orderRespository.findAll().forEach(order -> {orders.add(order);});



            response.put("orders", orders);
            response.put("currentPage", pageOrder.getNumber());
            response.put("totalItems", pageOrder.getTotalElements());
            response.put("totalPages", pageOrder.getTotalPages());

        } else {
            List<Order> orders = new ArrayList<Order>();

            Pageable paging = PageRequest.of(page, size);

            pageOrder = orderRespository.findAll(paging);
            orders = pageOrder.getContent();

            response.put("orders", orders);
            response.put("currentPage", pageOrder.getNumber());
            response.put("totalItems", pageOrder.getTotalElements());
            response.put("totalPages", pageOrder.getTotalPages());
        }


        return response;
    }

//    public List<Order> getAllOrders() {
//        List<Order> listOrder = new ArrayList<>();
//        orderRespository.findAll().forEach(order -> {listOrder.add(order);});
//        return listOrder;
//    }

    public Order createOrder(Long user_id, Order order) {
        return userRepository.findById(user_id).map(user -> {
            order.setUser(user);
//            Order newOrder = new Order(order.getId(), order.getPrice(), order.getName_product(), order.getQuantity());
            return orderRespository.save(order);
        }).orElseThrow();
    }

    public void deleteOrderById(Long user_id, Long order_id) {
        orderRespository.findByIdAndUserId(user_id, order_id).map(order ->
        {orderRespository.delete(order);
        return ResponseEntity.ok().build();
        }
        );
    }

    public Order updateOrderById(Long user_id, Long order_id, Order orders) {
        if (!userRepository.existsById(user_id)) {
            System.out.println("cannot find Id!");
        }
        return orderRespository.findByIdAndUserId(user_id, order_id).map(order -> {
            order.setName_product(orders.getName_product());
            order.setPrice(orders.getPrice());
            order.setQuantity(orders.getQuantity());
            return orderRespository.save(order);
        }).orElseGet(() -> {return orderRespository.save(orders);});
    }
}
