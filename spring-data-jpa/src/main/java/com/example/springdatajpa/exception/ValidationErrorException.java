package com.example.springdatajpa.exception;

public class ValidationErrorException extends RuntimeException {
    public ValidationErrorException() {
        super();
    }

    public ValidationErrorException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public ValidationErrorException(String message) {
        super(message);
    }

    public ValidationErrorException(Throwable throwable) {
        super(throwable);
    }
}
