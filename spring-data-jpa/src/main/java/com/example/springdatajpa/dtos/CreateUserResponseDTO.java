package com.example.springdatajpa.dtos;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class CreateUserResponseDTO implements Serializable {
    private Long id;
    private String name, email;
    private Integer age;
}
