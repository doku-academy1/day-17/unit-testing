package com.example.springdatajpa.dtos;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;


@Getter
@Setter
public class CreateUserRequestDTO implements Serializable {
    private String name, email;
    private Integer age;
}
